class racional:
    def __init__(self,x,y):
        self.x = x
        self.y = y

    def__add__(self, other):
        if self.y == other.y:
            return racional(self.x + other.x, self.y)
        else: 
            return racional(self.x*other.y + other.x*self.y , self.y*other.y)

    def __sub__(self, other):
        if self.y == other.y:
            return racional(self.x - other.x, self.y)
        else: 
            return racional(self.x*other.y - other.x*self.y, self.y*other.y)

    def __mul__(self, other):
        return racional(self.x * other.x, self.y * other.y)

    def __truediv__(self, other):
        return racional(self.x * other.y, self.y * other.x)